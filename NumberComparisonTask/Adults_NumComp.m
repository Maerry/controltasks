% Human task
% V 2.0 this version contains colored dots like the kid version
% contains practice trials and stores data in a local data folder.  Screen
% 1 used for presentation.
function Adults_NumComp

global exp

sub = exp.subj;
% Determine when summary feedbacks happen
exp.summary_feedback_trial.number = ceil(exp.numb_of_trials.number / exp.number_of_summary_feedbacks);
summaryFeedbackTrials = mod(1:exp.numb_of_trials.number, exp.summary_feedback_trial.number) == 0;

% Workaround for failing screen synching
Screen('Preference', 'SkipSyncTests', 1);

try
    if ~exist('sub','var')
        sub = 999;
    end
    
    folder.funPath = which('Adults_NumBias');
    folder.root = folder.funPath(1:strfind(folder.funPath,'Adults_NumBias.m')-1);
    folder.data = sprintf('%s',folder.root,'data');
    folder.stim = sprintf('%s',folder.root,'stimuli');
    curtime = clock;
%     folder.savefname = sprintf('%02d_adultNumBias.mat',sub);
%     folder.savepath = sprintf('%s',folder.data,filesep,folder.savefname);
    folder.savefname = sprintf('NUM_%s_%d_%s.mat', exp.subj, exp.session, exp.version);
    folder.savepath = [exp.results_filepath, 'data\', folder.savefname];
    folder.stimInfo = sprintf('%s',folder.stim,filesep,'559_stim.mat');
    folder.pairInfo = sprintf('%s',folder.stim,filesep,'559_pairs_info.mat');
    sess.folder = folder;
    
%     curtime = clock;
%     folder.stimInfo = sprintf('%s',folder.stim,filesep,'559_stim.mat');
%     folder.pairInfo = sprintf('%s',folder.stim,filesep,'559_pairs_info.mat');
%     sess.folder = folder;
    
    % Set up Matlab stuff
    % Seed the random number stream
    stream0 = RandStream('mt19937ar','Seed', sum(clock));
    %RandStream.setDefaultStream(stream0);
    ListenChar(2); % hide keyboard input
    KbName('UnifyKeyNames');
    stimcolor = hsv(32);
    
    % load stim info
    load(folder.stimInfo)
    load(folder.pairInfo)
    
    % session variables
    sess.debug = 0;
    sess.totalTrials = exp.numb_of_trials.number;
    sess.stimDispTime = .25;
    sess.giveFB = true;
    sess.pixelBuffer = 1; % minimum distance between dots in pixels.
    sess.uniDnumIndxToUse = [1 2 3 12];  % which ratios to show.  [1 2 3 6]
    % pick easier ratios for kids - want to target dynamic range + have one
    % real easy one
    sess.practiceTrials = true;
    % corresponds to the hardest 3 ratios plus a 1:2.  This variable
    % indexes uniDnum
    sess.sub = sub;
    sess.ITI = 2;
    sess.LKey = KbName('f');
    sess.RKey = KbName('j');
    
    % other variables/ initialization
    continue_running = 1;
    practiceTN = 0;
    trialNum = 0;
    uniDnum = unique(allpair.Dnum);
    varFieldIndx = {'lowVar','midVar','highVar'};
    if sess.practiceTrials; continue_practice = true; end;
    %%%%%%%%%%% initialize data! %%%%%%%%%%%%%
    data.tt = zeros(1,sess.totalTrials);
    data.Dnum = zeros(1,sess.totalTrials);
    data.DONSZ = zeros(1,sess.totalTrials);
    data.DONSP = zeros(1,sess.totalTrials);
    data.pairIndx = zeros(1,sess.totalTrials);
    data.S1ID = zeros(1,sess.totalTrials);
    data.S2ID = zeros(1,sess.totalTrials);
    data.S1side = zeros(1,sess.totalTrials);
    data.S2side = zeros(1,sess.totalTrials);
    data.stimGenAndSaveTime = zeros(1,sess.totalTrials);
    data.STfixOnset = zeros(1,sess.totalTrials);
    data.STstimOnset = zeros(1,sess.totalTrials);
    data.STrespArrowOnset = zeros(1,sess.totalTrials);
    data.respKey = zeros(1,sess.totalTrials);
    data.STrespMade = zeros(1,sess.totalTrials);
    data.correct = zeros(1,sess.totalTrials);
    data.STfeedBack = zeros(1,sess.totalTrials);
    data.S1centerOR = cell(1,sess.totalTrials);
    data.S2centerOR = cell(1,sess.totalTrials);
    data.S1pts = cell(1,sess.totalTrials);
    data.S2pts = cell(1,sess.totalTrials);
    data.S1dots = cell(1,sess.totalTrials);
    data.S2dots = cell(1,sess.totalTrials);
    
    % setup practice trials
    prac.Dnum = [2 2 2 2 2 2 2 2];
    prac.DONSZ = [2 2 2 2 -2 -2 -2 -2];
    prac.DONSP = [2 -2 2 -2 2 -2 2 -2];
    prac.S1side = [1 1 0 0 1 1 0 0 1 1];
    prac.S2side = ~prac.S1side;
 
    for k = 1:8
        prac.pairIndx(k) = find(...
            allpair.Dnum == prac.Dnum(k) &...
            allpair.DONSZ == prac.DONSZ(k) &...
            allpair.DONSP == prac.DONSP(k));
        if numel(prac.pairIndx(k)) == 0; keyboard; end;
        
    end
    prac.S1ID = allpair.S1ID(prac.pairIndx);
    prac.S2ID = allpair.S2ID(prac.pairIndx);
    
    % scramble practice trials
    shuffleIndx = Shuffle(1:8);
    pracFields = fields(prac);
    for k = 1:numel(pracFields)
        prac.(pracFields{k}) = prac.(pracFields{k})(shuffleIndx);
    end
        
    % open a screen and set screen positions
    sca;
    ptr = max(Screen('Screens'));
    [w sr] = Screen('OpenWindow',ptr,[0 0 0]);
    origin = [sr(3) sr(4)]/2;
    originRect = [origin origin];
    fixCross = [[-4 -1 4 1] + originRect; [-1 -4 1 4] + originRect]';
    leftOR = [sr(3)/2 - 225 sr(4)/2 sr(3)/2 - 225 sr(4)/2];
    rightOR = [sr(3)/2 + 225 sr(4)/2 sr(3)/2 + 225 sr(4)/2];
    
    % response arrows   
    leftArrow.head = [0 100; 100 0; 100 200;0 100];
    leftArrow.head = leftArrow.head + [origin;origin;origin;origin] + [-400 -100;-400 -100;-400 -100;-400 -100];
    leftArrow.tail = [origin(1)-300 origin(2)-50 origin(1)-200 origin(2)+50];
    rightArrow.head = [100 100; 0 0; 0 200; 100 100];
    rightArrow.head = rightArrow.head + [origin;origin;origin;origin] + [300 -100;300 -100;300 -100;300 -100];
    rightArrow.tail = [origin(1)+200 origin(2)-50 origin(1)+300 origin(2)+50];
    leftArrow.text = sprintf('MORE DOTS ON LEFT');
    rightArrow.text = sprintf('MORE DOTS ON RIGHT'); 
    leftArrow.center = mean([leftArrow.tail(3) leftArrow.head(1)]);
    rightArrow.center = mean([rightArrow.tail(1) rightArrow.head(1)]);
    [nx, ny, textbounds] = DrawFormattedText(w, leftArrow.text,0,0,[0 0 0]);
    leftArrow.textSize = textbounds(3);
    [nx, ny, textbounds] = DrawFormattedText(w, rightArrow.text,0,0,[0 0 0]);
    rightArrow.textSize = textbounds(3);
    
    % Set font parameters
    Screen('TextFont',  w, 'Arial');
    Screen('TextSize',  w, 24);
    
    % practice insturctions here
    DrawFormattedText(w, 'Choose the side with the most dots!\n\nWait for the arrows to appear on the screen to make your response.\n\n To choose the left side, press "f". To choose the right side, press "j".\n\n Press any key to continue...','center','center',[128 128 128]);% incorrect message
    Screen('Flip',w);
    [keyIsDown] = KbCheck;
    while keyIsDown
        [keyIsDown] = KbCheck;
        pause(0.01)
    end
    while ~keyIsDown
        [keyIsDown] = KbCheck;
        pause(0.01)
    end
    Screen('Flip',w);

    % start practice
    while continue_practice
        
        practiceTN = practiceTN + 1;
        
        if prac.S1side(practiceTN) == 1
            prac.S1centerOR{practiceTN} = rightOR;
            prac.S2centerOR{practiceTN} = leftOR;
        elseif prac.S1side(practiceTN) == 0
            prac.S1centerOR{practiceTN} = leftOR;
            prac.S2centerOR{practiceTN} = rightOR;
        end
        
        
        % gen prac stim
        % s1
        prac.S1pts{practiceTN} = dotFieldGKA(round(allstim.num(prac.S1ID(practiceTN))),... % numerosity
            round(sqrt(allstim.fa(prac.S1ID(practiceTN))/pi)*2),... % field diameter
            round(sqrt(allstim.ia(prac.S1ID(practiceTN))/pi)*2) + sess.pixelBuffer); % distance btwn dot centers
        prac.S1dots{practiceTN} = pts2rect(prac.S1pts{practiceTN},... % pts
            sqrt(allstim.ia(prac.S1ID(practiceTN))/pi),... % radius (can be vector if we do heterogenious dots)
            prac.S1centerOR{practiceTN},... % center of array in rect format (either left or right from coin flip above)
            round(sqrt(allstim.fa(prac.S1ID(practiceTN))/pi))); % radius of field
        % s2
        prac.S2pts{practiceTN} = dotFieldGKA(round(allstim.num(prac.S2ID(practiceTN))),...% numerosity
            round(sqrt(allstim.fa(prac.S2ID(practiceTN))/pi)*2),...% field diameter
            round(sqrt(allstim.ia(prac.S2ID(practiceTN))/pi)*2) + sess.pixelBuffer);% distance btwn dot centers
        prac.S2dots{practiceTN} = pts2rect(prac.S2pts{practiceTN},... % pts
            sqrt(allstim.ia(prac.S2ID(practiceTN))/pi),... % radius (can be vector if we do heterogenious dots)
            prac.S2centerOR{practiceTN},... % center of array in rect format (either left or right from coin flip above)
            round(sqrt(allstim.fa(prac.S2ID(practiceTN))/pi))); % radius of field
        
        % iti
        pause(2);
        
        % fixation cross
        Screen('FillRect',w,[128 128 128],fixCross);
        t = Screen('Flip',w);
        
        % draw stimuli
        curr_stimcolor = round(255*stimcolor(ceil(rand * length(stimcolor)),:));
        Screen('FillRect',w,[128 128 128],fixCross);
        Screen('FillOval',w,curr_stimcolor,prac.S1dots{practiceTN}');
        Screen('FillOval',w,curr_stimcolor,prac.S2dots{practiceTN}');
        while GetSecs - t < 1 && continue_practice
            continue_practice = escCheck;
            pause(0.01)
        end
        
        t = Screen('Flip',w);
        while GetSecs - t < 1 && continue_practice
            continue_practice = escCheck;
            pause(0.01)
        end
        
        Screen('FillRect',w,[128 128 128],fixCross);
        Screen('FillPoly',w,[128 128 128],rightArrow.head);
        Screen('FillRect',w,[128 128 128],rightArrow.tail);
        Screen('FillPoly',w,[128 128 128],leftArrow.head);
        Screen('FillRect',w,[128 128 128],leftArrow.tail);
        Screen('DrawText',w, leftArrow.text,leftArrow.center-leftArrow.textSize/2,origin(2)+120,[128 128 128]);
        Screen('DrawText',w, rightArrow.text,rightArrow.center-rightArrow.textSize/2,origin(2)+120,[128 128 128]);
        
        
        % show response arrows
        t = Screen('Flip',w);
        
        % wait for response
        waitingOnSub = true;
        while waitingOnSub && continue_practice
            continue_practice = escCheck;
            [keyIsDown, secs, keyCode] = KbCheck;
            if (sess.LKey || sess.RKey) && sum(keyCode) == 1;
                prac.respKey(practiceTN) = find(keyCode);
                prac.STrespMade(practiceTN) = secs;
                waitingOnSub = false;
            end
            pause(0.01)
        end
        
        % assess correct
        if continue_practice
            if (prac.respKey(practiceTN)-sess.LKey)/(sess.RKey-sess.LKey) == prac.S1side(practiceTN) % if the key indicates S1 the response was incorrect
                prac.correct(practiceTN) = 0;
                DrawFormattedText(w, 'Sorry, that was incorrect','center','center',[255 0 0]);% incorrect message
                Screen('Flip',w);
            elseif (prac.respKey(practiceTN)-sess.LKey)/(sess.RKey-sess.LKey)== prac.S2side(practiceTN) % if the key indicates S2 the response was correct
                prac.correct(practiceTN) = 1;
                DrawFormattedText(w, 'CORRECT!','center','center',[0 255 0]);% correct message
                Screen('Flip',w);
            else 
                prac.correct(practiceTN) = nan;
                DrawFormattedText(w, 'Wrong key! Please choose by pressing "f" or "j".','center','center',[255 255 255]);% correct message
                Screen('Flip',w);    
            end
            WaitSecs(1);
%             % give feedback
%             if ~prac.correct(practiceTN) % abort if there is an incorrect response
%                 prac.STfeedBack(practiceTN) = Screen('Flip',w);
%                 continue_practice = false;
%                 continue_running = false;
%                 warning('Incorrect practice trial');
%                 [keyIsDown] = KbCheck;
%                 while keyIsDown
%                     [keyIsDown] = KbCheck;
%                     pause(0.01)
%                 end
%                 while ~keyIsDown
%                     [keyIsDown] = KbCheck;
%                     pause(0.01)
%                 end
%             else
%                 Screen('FillRect',w,[0 0 0]);
%                 prac.STfeedBack(practiceTN) = Screen('Flip',w);
%             end
        end
        
        % quit check
        if practiceTN >= 8
            continue_practice = 0;
        end
    end
    
    % regular instructions here
    DrawFormattedText(w, 'The practice is now over.\n\nReady to begin?\n\nPress any key to continue...','center','center',[128 128 128]);% incorrect message
    Screen('Flip',w);
    while keyIsDown
        [keyIsDown] = KbCheck;
        pause(0.01)
    end
    while ~keyIsDown
        [keyIsDown] = KbCheck;
        pause(0.01)
    end
    
    % start task
    points = 0;
    while continue_running
        
        trialNum = trialNum + 1;
        
        % pick a pair of stimuli and assigne sides.
        % stimulus pair attributes are assigned below.  these are the
        % attributes that we want even distributions of (ratio and variance
        % level).
%         data.tt(trialNum) = ceil(rand*3);
        data.Dnum(trialNum) = uniDnum(sess.uniDnumIndxToUse(ceil(rand*length(sess.uniDnumIndxToUse))));
        % the crazy code above randomly picks a Dnum from a subset of all
        % the unique Dnums.  This biases the stimuli towards harder
        % discriminations.
        numindx = allpair.Dnum == data.Dnum(trialNum);
        uniDONSZ = unique(allpair.DONSZ(numindx));
        uniDONSP = unique(allpair.DONSP(numindx));
        data.DONSZ(trialNum) = uniDONSZ(ceil(rand*numel(uniDONSZ)));
        data.DONSP(trialNum) = uniDONSP(ceil(rand*numel(uniDONSP)));
        
        % indx picks out the stimulus pairs that meet the criteria above. the
        % pair is then drawn randomly from all those pairs meet the criteria
%         indx = find(allpair.(varFieldIndx{data.tt(trialNum)}) &...
%             allpair.Dnum == data.Dnum(trialNum));
        indx = find(...
            allpair.Dnum == data.Dnum(trialNum) &...
            allpair.DONSZ == data.DONSZ(trialNum) &...
            allpair.DONSP == data.DONSP(trialNum));
        randIndx = ceil(rand*length(indx));
        data.pairIndx(trialNum) = indx(randIndx);
        data.S1ID(trialNum) = allpair.S1ID(data.pairIndx(trialNum));
        data.S2ID(trialNum) = allpair.S2ID(data.pairIndx(trialNum));
        data.S1side(trialNum) = round(rand); % 0 is left 1 is right
        data.S2side(trialNum) = ~data.S1side(trialNum);
        if data.S1side(trialNum) == 1
            data.S1centerOR{trialNum} = rightOR;
            data.S2centerOR{trialNum} = leftOR;
        elseif data.S1side(trialNum) == 0
            data.S1centerOR{trialNum} = leftOR;
            data.S2centerOR{trialNum} = rightOR;
        end
        
        % gen stim
        % s1
        data.S1pts{trialNum} = dotFieldGKA(round(allstim.num(data.S1ID(trialNum))),... % numerosity
            round(sqrt(allstim.fa(data.S1ID(trialNum))/pi)*2),... % field diameter
            round(sqrt(allstim.ia(data.S1ID(trialNum))/pi)*2) + sess.pixelBuffer); % distance btwn dot centers
        data.S1dots{trialNum} = pts2rect(data.S1pts{trialNum},... % pts
            sqrt(allstim.ia(data.S1ID(trialNum))/pi),... % radius (can be vector if we do heterogenious dots)
            data.S1centerOR{trialNum},... % center of array in rect format (either left or right from coin flip above)
            round(sqrt(allstim.fa(data.S1ID(trialNum))/pi))); % radius of field
        % s2
        data.S2pts{trialNum} = dotFieldGKA(round(allstim.num(data.S2ID(trialNum))),...% numerosity
            round(sqrt(allstim.fa(data.S2ID(trialNum))/pi)*2),...% field diameter
            round(sqrt(allstim.ia(data.S2ID(trialNum))/pi)*2) + sess.pixelBuffer);% distance btwn dot centers
        data.S2dots{trialNum} = pts2rect(data.S2pts{trialNum},... % pts
            sqrt(allstim.ia(data.S2ID(trialNum))/pi),... % radius (can be vector if we do heterogenious dots)
            data.S2centerOR{trialNum},... % center of array in rect format (either left or right from coin flip above)
            round(sqrt(allstim.fa(data.S2ID(trialNum))/pi))); % radius of field
        
        
        % iti
        % the iti occurs at the beginning of the trial which is weird
        t = GetSecs;
        if trialNum == 1;
            data.stimGenAndSaveTime(trialNum) = 0;
            while GetSecs - t < sess.ITI && continue_running
                continue_running = escCheck;
            end
        elseif trialNum > 1;
            data.stimGenAndSaveTime(trialNum) = (t - data.STfeedBack(trialNum-1));
            if data.stimGenAndSaveTime(trialNum) > sess.ITI
                warning('Saving data and generating stimuli is taking longer than the preset ITI and is slowing down the task.  If this problem is persistant it may indicate task slowing over time.')
            end
            while GetSecs - data.STfeedBack(trialNum-1) < sess.ITI && continue_running
                continue_running = escCheck;
            end
        end
        
        % fixation cross
        Screen('FillRect',w,[128 128 128],fixCross);
        data.STfixOnset(trialNum) = Screen('Flip',w); % this is the official start of the trial
        
        % draw stimuli
        curr_stimcolor = round(255*stimcolor(ceil(rand * length(stimcolor)),:));
        Screen('FillRect',w,[128 128 128],fixCross);
        Screen('FillOval',w,curr_stimcolor,data.S1dots{trialNum}');
        Screen('FillOval',w,curr_stimcolor,data.S2dots{trialNum}');
        while GetSecs - data.STfixOnset(trialNum) < 0.5 && continue_running
            continue_running = escCheck;
        end
        
        % show stimuli
        data.STstimOnset(trialNum) = Screen('Flip',w);
%         data.trialTime.stimOnset(trialNum) = data.STstimOnset(trialNum) - data.STfixOnset(trialNum);
        
        % draw response arrows
        Screen('FillRect',w,[128 128 128],fixCross);
        Screen('FillPoly',w,[128 128 128],rightArrow.head);
        Screen('FillRect',w,[128 128 128],rightArrow.tail);
        Screen('FillPoly',w,[128 128 128],leftArrow.head);
        Screen('FillRect',w,[128 128 128],leftArrow.tail);
        Screen('DrawText',w, leftArrow.text,leftArrow.center-leftArrow.textSize/2,origin(2)+120,[128 128 128]);
        Screen('DrawText',w, rightArrow.text,rightArrow.center-rightArrow.textSize/2,origin(2)+120,[128 128 128]);
        
        % show stim info for debugging
        if sess.debug
            fprintf('%%%%%%%%%%%%%%%%\nS1 = %d S2 = %d \nDnum = %0.2f\nDONSZ = %0.2f DONSP = %0.2f\n',...
                round(allstim.num(data.S1ID(trialNum))),round(allstim.num(data.S2ID(trialNum))),...
                allpair.Dnum(data.pairIndx(trialNum)),...
                allpair.DONSZ(data.pairIndx(trialNum)),allpair.DONSP(data.pairIndx(trialNum)))
        end
        
        % stim on wait
        if sess.debug
            debugWait = true;
            while debugWait && continue_running
                continue_running = escCheck;
                [keyDown] = KbCheck;
                if keyDown; debugWait = false; end;
            end
        else
            while GetSecs - data.STstimOnset(trialNum) < sess.stimDispTime && continue_running
                continue_running = escCheck;
            end
        end
        
        % show response arrows
        data.STrespArrowOnset(trialNum) = Screen('Flip',w);
        
        % wait for response
        waitingOnSub = true;
        while waitingOnSub && continue_running
            continue_running = escCheck;
            [keyIsDown, secs, keyCode] = KbCheck;
            if (sess.LKey || sess.RKey) && sum(keyCode) == 1;
                data.respKey(trialNum) = find(keyCode);
                data.STrespMade(trialNum) = secs;
                waitingOnSub = false;
            end
            pause(0.01)
        end
        
        % assess correct
        if continue_running
            if (data.respKey(trialNum)-sess.LKey)/(sess.RKey-sess.LKey) == data.S1side(trialNum) % if the key indicates S1 the response was incorrect
                data.correct(trialNum) = 0;
                DrawFormattedText(w, 'WRONG','center','center',[255 0 0]);% incorrect message
            elseif (data.respKey(trialNum)-sess.LKey)/(sess.RKey-sess.LKey) == data.S2side(trialNum) % if the key indicates S2 the response was correct
                data.correct(trialNum) = 1;
                points = points + 1;
                DrawFormattedText(w, 'CORRECT!','center','center',[0 255 0]);% correct message
            else 
                data.correct(trialNum) = nan;
                DrawFormattedText(w, 'Wrong key! Please choose by pressing "f" or "j".','center','center',[255 255 255]);% correct message
            end
            
            % give feedback
            if sess.giveFB
                data.STfeedBack(trialNum) = Screen('Flip',w);
            else
                Screen('FillRect',w,[0 0 0]);
                data.STfeedBack(trialNum) = Screen('Flip',w);
            end
        end
        
        
        % save data
        save(folder.savepath,'data','sess','prac');
        
        % give summary feedback
        if summaryFeedbackTrials(trialNum)
            dispSummaryFeedback(points, w)
            points = 0;
        end
        
        % quit check
        if trialNum >= sess.totalTrials
            continue_running = 0;
        end
            
    end %ctn running
    
    received_points = nansum(data.correct);
    pr_money_bonus(received_points, w)
    
    sca;
catch ME
    keyboard
end %try
end %fxn


%% More functions

function dots = pts2rect(pts,dotRads,centerRect,fieldRad)
    if numel(dotRads) == 1
        dotRads = dotRads * ones(numel(pts)/2,1);
    elseif numel(dotRads) ~= numel(pts)/2
        error('wrong number of dot rads')
    end
    fieldRadRect = [fieldRad fieldRad fieldRad fieldRad];
    dots = [pts pts] + [-dotRads -dotRads dotRads dotRads] + repmat(centerRect,numel(dotRads),1) -  repmat(fieldRadRect,numel(dotRads),1);
    %     data.dots{trialNum}(1,:) = data.S1pts{trialNum}(:,1) - sqrt(allstim.ia(data.S1ID(trialNum))/pi) + origin(1) - round(sqrt(allstim.fa(data.S1ID(trialNum))/pi));
    %     data.dots{trialNum}(2,:) = data.S1pts{trialNum}(:,2) - sqrt(allstim.ia(data.S1ID(trialNum))/pi) + origin(2) - round(sqrt(allstim.fa(data.S1ID(trialNum))/pi));
    %     data.dots{trialNum}(3,:) = data.S1pts{trialNum}(:,1) + sqrt(allstim.ia(data.S1ID(trialNum))/pi) + origin(1) - round(sqrt(allstim.fa(data.S1ID(trialNum))/pi));
    %     data.dots{trialNum}(4,:) = data.S1pts{trialNum}(:,2) + sqrt(allstim.ia(data.S1ID(trialNum))/pi) + origin(2) - round(sqrt(allstim.fa(data.S1ID(trialNum))/pi));
end

function continue_running = escCheck
    [keyIsDown secs keyCode] = KbCheck;
    if keyIsDown && keyCode(41)
        continue_running = 0;
    else
        continue_running = 1;
    end
end

function pts = dotFieldGKA(ndots, fieldSize, interDot)
% dotFieldGKA   Generate a random dot field
% Usage:
%     pts = dotFieldGKA(ndots, fieldSize, interDot)
%   Specify a number of points within a circular field that are positioned
% randomly, subject only to the constraint that they must be some distance
% from each other.  These points can be used as the centers of circular
% dots for a field of random, non-overlapping dots.
%   ndots is the number of points to generate.
%   fieldSize is the diameter of the field within which the points are to
% be generated, specified in pixels.
%   interDot is the minimum distance between points, specified in pixels.
% To produce non-overlapping dots, this value could be the diameter of the
% dots plus a small buffer.
%   pts is an ndots-by-2 matrix, such that pts(k,1) is the x coordinate of
% the k'th dot, and pts(k,2) is its y coordinate. Note that this is
% coordinates within the dot field; to convert to screen coordinates for
% drawing, the upper left-hand coordinate of the dot field should be added
% to the pts values.
%
% 2009/12/25 GKA
%
% interdot can now be a vector of length ndots (or still a simple scalar),
% which allows dots of different sizes within a single field. When a vector
% is used each value is the dot radius.  when a scalar is used interDot is
% still the diameter plus a buffer.

% Also replaced 'dummy' variable with a '~'

% 2010/7/27 NKD

if length(interDot) > 1 && length(interDot) ~= ndots
    error('interDot~=ndots','interdot is not scalar and does not equal ndots')
end

field = rand(fieldSize);
[Y, X] = meshgrid(0:(fieldSize-1));
rad = (fieldSize-1) / 2;
outField = sqrt((X-rad).^2 + (Y-rad).^2) > rad;
field(outField) = 0;

field = field(:);
X = X(:);
Y = Y(:);

pts = zeros(ndots, 2);

siz = [fieldSize fieldSize];

buffer = 2;

for k=1:ndots
    currField=field;
    if length(interDot) > 1
        for i=1:k-1
            tooClose = sqrt((X - (pts(i,1)-1)).^2 + (Y - (pts(i,2)-1)).^2)...
                <= interDot(k)+interDot(i)+buffer;
            currField(tooClose)=0;
        end
        [asdf, I] = max(currField);
        [xk, yk] = ind2sub(siz, I);
        pts(k,:) = [xk yk];
        if ~any(currField)
            error('dotFieldGKA:noSpace', 'Can''t fit in any more dots!');
            break;
        end
    else
        [asdf, I] = max(field);
        [xk, yk] = ind2sub(siz, I);
        tooClose = sqrt((X - (xk-1)).^2 + (Y - (yk-1)).^2) <= interDot;
        field(tooClose) = 0;
        pts(k,:) = [xk yk];
        if ~any(field)
            error('dotFieldGKA:noSpace', 'Can''t fit in any more dots!');
            break;
        end
    end
end
end

function dispSummaryFeedback(points, w)

global exp

%% Write and display the message
m1 = ['You got ', num2str(points), ' of the last ', num2str(exp.summary_feedback_trial.number), ' trials right! '];
m2 = ' Press space to resume the task.';

DrawFormattedText(w, [m1 '\n\n' m2], 'center','center', [128 128 128])
Screen('Flip', w);
WaitSecs(1);
KbWait;

end

function pr_money_bonus(received_points, w)

global exp

%%% Calculate task bonus
min_points      = exp.numb_of_trials.number / 2;   % Chance
if received_points < min_points
    min_points = received_points;                        
end
max_points      = exp.numb_of_trials.number;       % If subject gets every trial right

bonus = (received_points - min_points) / (max_points - min_points) * ...
    exp.bonus.max_session/6;

%%% Store task bonus (to calculate total bonus at the end)
if any(strcmp(exp.version, {'A', 'C'}))
    exp.bonus.task1_run1 = round(bonus, 2);
elseif any(strcmp(exp.version, {'B', 'D'}))
    exp.bonus.task1_run2 = round(bonus, 2);
end

m11 = 'Great job!';
m12 = ['You achieved a bonus of $', num2str(round(bonus, 2)), ' in this task!'];
m13 = '(This bonus is based on your performance and added';
m14 = 'to your regular payment.)';
m15 = 'Press space when you want to move on to your next task.';

DrawFormattedText(w, [m11 '\n\n\n\n\n' m12 '\n\n\n' m13 '\n\n' m14 '\n\n\n\n\n' m15], 'center','center', [128 128 128])
Screen('Flip', w);
WaitSecs(1);
KbWait;

end