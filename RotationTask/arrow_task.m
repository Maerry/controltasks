function arrow_task

global exp

%% Run the task                  
init_arrow
set_buffers_and_messages

%%% Give task instructions
pr_instruction;

for trial = 1:exp.numb_of_trials.rotation
    init_trial(trial);                                                      % Clear buffers, prepare buffers, and initialize truth values
    
    %%% Regular task trial
    pr_arrows(trial);
    exp.ACC = pr_feedback(trial);
    
    %%% Insert catch question
    if exp.catch_trials(trial) == 1
        exp.catch_ACC = pr_catch_trial(trial);
    end
        
    rec_trial(trial);                                                       % Record trial information
    
    %%% Insert summary feedback
    if exp.catch_trials(trial) == 5
        pr_summary_feedback(trial);
    end
    
end

%% Say goodbye
pr_money_feedback
stop_cogent;
