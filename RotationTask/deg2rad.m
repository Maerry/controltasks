function rad = deg2rad(deg)

rad = deg * 2*pi / 180;

end