function init_arrow

global exp;

%% Configure Cogent
% Get background color (background of Gabor patches)
[~, ~, Gex] = gabor('theta', deg2rad(55), 'labmda', 20, 'sigma', 15, 'px', .5, 'py', .5);
Gex = Gex - min(min(Gex));
Gex = Gex / max(max(Gex));
back_color = Gex(1);

% Configure display & keyboard
config_display(1, 3, [back_color back_color back_color], [1 1 1], 'Helvetica', 40, 10, 0);   % Configure display (0 = window mode; 5 = 1280x1024; [1 1 1] = white background; grey text; fontname; fontsize; nbuffers)
config_keyboard;

%% Set Variables
exp.wdir                = cd;                                                       % Working directory
exp.catch_position      = 1;

% Fix durations for everything in the experiment
exp.times.G1            =    50;
exp.times.G2            =    50;
exp.times.isi           =   500;
exp.times.cue           = 25000;                                                    % Duration of key press cue
exp.times.feedback      =   500;
exp.times.iti           = ones(1, exp.numb_of_trials.rotation);                     % Inter-trial interval [0.3 0.7] with mean 0.5
while mean(exp.times.iti) > .501 || mean(exp.times.iti) < .499
    exp.times.iti = 0.3 + 0.4 * rand(1, exp.numb_of_trials.rotation);
end

% Initialize log stuff
exp.ARROWdata.ACC             = zeros(1, exp.numb_of_trials.rotation);
exp.ARROWdata.RT              = [];
exp.ARROWdata.stime           = [];                                                       % Initialize
exp.ARROWdata.key             = [];                                                       % Initialize for data saving
exp.ARROWdata.keytime         = [];

% Set keys to select arrows and stimuli
exp.nkey.le          =  97;
exp.nkey.ri          =  98;

%% Orientation of the patterns (how many degrees of rotation in what trial?) 
exp.mean_rotation = zeros(1, exp.numb_of_trials.rotation);
while mean(exp.mean_rotation) > deg2rad(55) + .1 || mean(exp.mean_rotation) < deg2rad(55) - .1
    exp.mean_rotation = deg2rad(45) + deg2rad(20) * rand(1, exp.numb_of_trials.rotation);         % mean ~ 55; range = [45; 65]
end

exp.dev_rotation = [];
rotations = deg2rad([-10:4:-2, 2:4:10]);
for chunk = 1:idivide(exp.numb_of_trials.rotation, int32(6), 'ceil')
    exp.dev_rotation = ...                                                  % Difference in rotation of the 'odd' arrow
        [exp.dev_rotation, datasample(rotations, 6, 'Replace', false)];
end

%% Catch trials and feedback trials
exp.catch_key     =  [];
% Trials that will be catch trials
exp.catch_trials  = zeros(1, exp.numb_of_trials.rotation);
exp.catch_trials([5, 10, 23, 29, 40, 51, 59, 70, 83, 90]) = 1;
exp.summary_feedback_trial.rotation = ceil(exp.numb_of_trials.rotation / exp.number_of_summary_feedbacks);
exp.catch_trials(mod(1:exp.numb_of_trials.rotation, exp.summary_feedback_trial.rotation) == 0) = 5;
exp.catch_trials(exp.numb_of_trials.rotation) = 5;

% Presented numbers
exp.catch_numbers = exp.catch_trials;
exp.catch_numbers(exp.catch_numbers == 1) = datasample([6, 9, 3, 8], sum((exp.catch_trials == 1)), 'replace', true);

%% Positions on the screen
if exp.session == 1
    exp.p.arrows_x  =  .2;
    exp.p.arrows_y  =  .2;
elseif exp.session == 2
    exp.p.arrows_x  =  .8;
    exp.p.arrows_y  =  .2;
end

%% Try to start cogent (else, wait for enter - to get matlab window)
try
    start_cogent;
catch
    input('Please press enter to continue')
    start_cogent;
end