function init_trial(trial)

global exp;

%% Clear necessary buffers and keys
clearkeys;

%% Initialize truth values
exp.ACC                 = nan;
exp.key                 = [];
exp.keytime             = [];
exp.n                   = 0;
exp.catch_ACC           = nan;

%% Prepare the arrow/Gabor pictures (normal arrow & oddball arrow)
norm_rot = exp.mean_rotation(trial);                                        % Baseline rotation of the arrows for this trial
odd_rot  = exp.mean_rotation(trial) + exp.dev_rotation(trial);              % Rotation of the 'oddball' arrow

%% Info about gabor function:
raw_lambda = 20;                                                            % lambda (20): spatial wavelength
raw_sigma = 50;                                                             % Sigma (10): standard deviation of gaussian window
lambda = normrnd(raw_lambda, raw_lambda/10);
sigma = normrnd(raw_sigma, raw_sigma/10);
% theta (0:2*pi): orientation of the gabor patch
% width:  width of generated image
% height: height of generated image
% px:     horizontal center of gabor patch, relative to the image width (must be between 0 and 1)
% py:     vertical center of gabor patch, relative to the image height (must be between 0 and 1)

%% Load Gabor patterns into buffers
[~, ~, G1] = gabor('theta', norm_rot, 'lambda', lambda, 'sigma', sigma, ... 
                   'width', 1280, 'height', 1024, 'px', exp.p.arrows_x, 'py', exp.p.arrows_y);
G1 = G1 - min(min(G1));
G1 = G1 / max(max(G1));

[~, ~, G2] = gabor('theta', odd_rot, 'lambda', lambda, 'sigma', sigma, ... 
                   'width', 1280, 'height', 1024, 'px', exp.p.arrows_x, 'py', exp.p.arrows_y);
G2 = G2 - min(min(G2));                                                     % Normalize resulting Gabor pattern
G2 = G2 / max(max(G2));

clearpict(exp.buffer.G1);
clearpict(exp.buffer.G2);
preparepict(G1, exp.buffer.G1);
preparepict(G2, exp.buffer.G2);
preparestring('+', exp.buffer.G1);
preparestring('+', exp.buffer.G2);

%% Load catch number into buffer (only displayed in actual catch trials)
clearpict(exp.buffer.catch_trial);
preparestring(num2str(exp.catch_numbers(trial)), exp.buffer.catch_trial);

end
