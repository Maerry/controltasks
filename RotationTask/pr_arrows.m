function pr_arrows(trial)

global exp

%% First Gabor pattern
drawpict(exp.buffer.G1);
wait(exp.times.G1);

%% Blank screen / catch number
%%% Catch trial
if exp.catch_trials(trial) == 1
    % fixation
    drawpict(exp.buffer.fixation);
    wait((exp.times.isi - 50)/2);
    % 50 ms number
    drawpict(exp.buffer.catch_trial);
    wait(50);
    % fixation
    drawpict(exp.buffer.fixation);
    wait((exp.times.isi - 50)/2);
%%% Not a catch trial
else
    % fixation
    drawpict(exp.buffer.fixation)
    wait(exp.times.isi);
end

%% Second Gabor pattern
drawpict(exp.buffer.G2);
wait(exp.times.G2);
exp.t = drawpict(exp.buffer.fixation);
[exp.key, exp.keytime, exp.n] = waitkeydown(exp.times.cue);                 % Wait for response and get identity and time of pressed key

end
