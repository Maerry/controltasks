function ACC = pr_catch_trial(trial)

global exp

%% Prepare text
m1 = 'CATCH TRIAL!';
m2 = 'What number was presented at the fixation cross?';
m3 = 'Type the number on your keyboard (please use the number row).';

%% Ask for the number
clearpict(exp.buffer.catch_trial)
preparestring(m1, exp.buffer.catch_trial, 0, 270);
preparestring(m2, exp.buffer.catch_trial, 0,  50);
preparestring(m3, exp.buffer.catch_trial, 0, -50);
drawpict(exp.buffer.catch_trial);

%% Record response and determine accuracy
exp.catch_t = drawpict(exp.buffer.catch_trial);
[exp.catch_key, exp.catch_keytime, ~] = waitkeydown(exp.times.cue);         % Wait for response and get identity and time of pressed key

%% Give feedback
if isempty(exp.catch_key)
    drawpict(exp.buffer.no_response)
    waitkeydown(4 * exp.times.feedback)
    ACC = nan;
elseif (exp.catch_key - 27) == exp.catch_numbers(trial)                     % Numbers of keys in number row: 28-36
    drawpict(exp.buffer.correct_response)
    waitkeydown(2 * exp.times.feedback)
    ACC = 1;
else
    drawpict(exp.buffer.false_response)
    waitkeydown(4 * exp.times.feedback)
    ACC = 0;
end

end
