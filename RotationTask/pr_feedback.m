function ACC = pr_feedback(trial)

global exp

%% Determine which key is the correct one
if exp.dev_rotation(trial) < 0
    cor_key = exp.nkey.le;
    fal_key = exp.nkey.ri;
elseif exp.dev_rotation(trial) > 0
    cor_key = exp.nkey.ri;
    fal_key = exp.nkey.le;
end

%% Determine accuracy and present feedback
if ~isempty(exp.key)
    if exp.key == cor_key
        ACC = 1;
        drawpict(exp.buffer.correct_response);
        waitkeydown(exp.times.feedback)
    elseif exp.key == fal_key
        ACC = 0;
        drawpict(exp.buffer.false_response);
        waitkeydown(2 * exp.times.feedback)
    else
        ACC = nan;
        drawpict(exp.buffer.wrong_key)
        waitkeydown(2 * exp.times.feedback)
    end
else
    ACC = nan;
    drawpict(exp.buffer.no_response)
    waitkeydown(2 * exp.times.feedback)
end

%% Wait ITI
drawpict(exp.buffer.fixation);                                                                % Fixation screen
wait(exp.times.iti(trial));

end
