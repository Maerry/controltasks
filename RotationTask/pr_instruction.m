function pr_instruction

global exp;

upper_end = 300;

%% Write messages
if any(strcmp(exp.version, {'A', 'C'}))
    if strcmp(exp.data.language, 'English')
        m11 = 'In the following task, you will compare two patterns.';
        m12 = 'The patterns will appear one after the other like this:';
        m21 = 'If the second pattern is oriented CLOCKWISE';
        m22 = 'with respect to the first,';
        m23 = 'you need to press the RIGHT arrow key.';
        m24 = 'If the second is oriented COUNTER-CLOCKWISE';
        m25 = 'with respect to the first,';
        m26 = 'you need to press the LEFT arrow key.';
        m31 = 'Any questions?';
        m41 = 'ATTENTION!';
        m42 = 'You absolutely need to look at the FIXATION CROSS';
        m43 = 'in the center of the screen AT ALL TIMES!';
        m44 = 'NEVER move your eyes away from the center, even though';
        m45 = 'the patterns will appear at the side.';
        m46 = 'There will be catch trials throughout the task.';
        m47 = 'to make sure you are fixating on the cross.';
        m51 = 'Do you have any questions?';
        m52 = 'Press the space key when you want to start.';
    else   % exp.data.language is not 'English'
        m11 = 'In the following task, you will compare two patterns';
        m12 = 'The patterns will appear one after the other like this:';
        m21 = 'If the second pattern is oriented CLOCKWISE';
        m22 = 'with respect to the first,';
        m23 = 'you need to press the RIGHT arrow key.';
        m24 = 'If the second is oriented COUNTER-CLOCKWISE,';
        m25 = 'with respect to the first,';
        m26 = 'you need to press the LEFT arrow key.';
        m31 = 'Any questions?';
        m41 = 'ATTENTION!';
        m42 = 'You absolutely need to look at the FIXATION CROSS';
        m43 = 'in the center of the screen AT ALL TIMES!';
        m44 = 'NEVER move your eyes away from the center, even though';
        m45 = 'the patterns will appear at the edge of the screen.';
        m46 = 'This is really important.';
        m51 = 'Do you have any questions?';
        m52 = 'Press the space key when you want to start.';
    end

    %% slide 1
    clearkeys;
    exp.key = [];
    while isempty(exp.key)
        clearpict(1)
        preparestring(m11, 1, 0, 90);
        preparestring(m12, 1, 0, 20);
        loadpict('stimuli/Gabor50.png', 1, 0, -100);
        drawpict(1)
        wait(100)
        clearpict(1)
        preparestring(m11, 1, 0, 90);
        preparestring(m12, 1, 0, 20);
        drawpict(1)
        wait(500)
        clearpict(1)
        preparestring(m11, 1, 0, 90);
        preparestring(m12, 1, 0, 20);
        loadpict('stimuli/Gabor55.png', 1, 0, -100);
        drawpict(1)
        wait(100)
        clearpict(1)
        preparestring(m11, 1, 0, 90);
        preparestring(m12, 1, 0, 20);
        drawpict(1)
        [exp.key, ~, ~] = waitkeydown(1000);
    end

    %% slide 2 a
    clearpict(1)
    preparestring(m21, 1, 0, upper_end- 50);
    preparestring(m22, 1, 0, upper_end-120);
    preparestring(m23, 1, 0, upper_end-190);
    drawpict(1);
    waitkeydown(inf, 71);
    loadpict('stimuli/right_turn.png', 1, 0, -150);
    drawpict(1);
    waitkeydown(inf, 71);

    %% slide 2 b
    clearpict(1)
    preparestring(m24, 1, 0, upper_end- 50);
    preparestring(m25, 1, 0, upper_end-120);
    preparestring(m26, 1, 0, upper_end-190);
    drawpict(1);
    waitkeydown(inf, 71);
    loadpict('stimuli/left_turn.png', 1, 0, -150);
    drawpict(1);
    waitkeydown(inf, 71);

    %% slide 3
    clearpict(1);
    preparestring(m31, 1)
    drawpict(1);
    waitkeydown(inf, 71);

    %% slide 4
    clearpict(1);
    preparestring(m41, 1, 0, upper_end);
    preparestring(m42, 1, 0, upper_end-100);
    preparestring(m43, 1, 0, upper_end-150);
    preparestring('+', 1);
    drawpict(1);
    waitkeydown(inf, 71)
    preparestring(m44, 1, 0, -150);
    preparestring(m45, 1, 0, -200);
    loadpict('stimuli/Gabor50.png', 1, -300, 0);
    drawpict(1);
    waitkeydown(inf, 71)
    preparestring(m46, 1, 0, -upper_end+50);
    preparestring(m47, 1, 0, -upper_end);
    drawpict(1);
    waitkeydown(inf, 71)

    %% last slide
    clearpict(1);
    preparestring(m51, 1, 0, upper_end);
    drawpict(1);
    waitkeydown(inf, 71)
    preparestring(m52, 1, 0, -upper_end);
    drawpict(1);
    waitkeydown(inf, 71)
    
elseif any(strcmp(exp.version, {'B', 'D'}))
    m11 = 'Welcome back to the orientation discrimination task!';
    m12 = 'Tell which way the second patterns is tilted';
    m13 = 'compared to the first, just like before!';
    m14 = 'The right arrow means clockwise, the left arrow counter-clockwise.';
    
    m21 = 'Do you have any questions? Please raise your hand if you do.';
    m22 = 'Press space when you are ready to start.';
    
    %% slide 4
    clearpict(1);
    preparestring(m11, 1, 0, upper_end);
    preparestring(m12, 1, 0, upper_end-100);
    preparestring(m13, 1, 0, upper_end-150);
    preparestring(m14, 1, 0, upper_end-250);
    drawpict(1);
    waitkeydown(inf, 71)
    preparestring(m21, 1, 0, -150);
    preparestring(m22, 1, 0, -200);
    drawpict(1);
    waitkeydown(inf, 71)
    
end

end
