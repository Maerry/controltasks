function pr_money_feedback

global exp

%% Calculate task bonus
received_points = nansum(exp.ARROWdata.ACC);
min_points      = exp.numb_of_trials.rotation / 2;   % Number of rewards if button is only pressed once per trial
max_points      = exp.numb_of_trials.rotation;   % Number of rewards if right button is pressed in every interval

if received_points < min_points
    received_points = min_points;
end

bonus = (received_points - min_points) / (max_points - min_points) * ...
    exp.bonus.max_session/6;

%% Store task bonus (to calculate total bonus at the end)
if any(strcmp(exp.version, {'A', 'C'}))
    exp.bonus.task2_run1 = bonus;
elseif any(strcmp(exp.version, {'B', 'D'}))
    exp.bonus.task2_run2 = bonus;
end

%% Prepare and present message
m11 = 'Great job!';
m12 = ['You achieved a bonus of $', num2str(round(bonus, 2)), ' in this task!'];
m13 = '(This bonus is based on your performance and added';
m14 = 'to your regular payment.)';
m15 = 'Press space when you want to move on to your next task.';

clearpict(5)
preparestring(m11, 5, 0, 270);
preparestring(m12, 5, 0, 200);
preparestring(m13, 5, 0, 150);
preparestring(m14, 5, 0, 100);
drawpict(5);
waitkeydown(inf, 71);
preparestring(m15, 5, 0,-250);
drawpict(5);
waitkeydown(inf, 71);
