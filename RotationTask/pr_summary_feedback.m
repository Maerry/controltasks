function pr_summary_feedback(trial)

global exp

%% Determine number of points won
%%% If there was no summary feedback yet
if ~any(exp.catch_trials(1:(trial-1)) == 5)
    % count rewards from the beginning of the task
    trial_with_last_feedback = 0;
    
%%% If there already was a summary feedback
else
    % count the rewards since then
    trial_with_last_feedback = find(exp.catch_trials(1:(trial-1)) == 5, 1, 'last');
end

%%% Count the rewards
trials_correct_since_last_feedback = nansum(exp.ARROWdata.ACC((trial_with_last_feedback+1):trial));

%% Write and display the message
m1 = ['You got ', num2str(trials_correct_since_last_feedback), ' of the last ', num2str(exp.summary_feedback_trial.rotation), ' trials right!'];
m2 = 'Press space to resume the task.';

cgpencol(0, 0, 0);
cgflip(0.7,0.7,0.7);
cgtext(m1, 0,  200); 
cgtext(m2, 0, -200); 
cgflip(0.7,0.7,0.7);
waitkeydown(inf, 71);


