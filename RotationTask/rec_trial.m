    function rec_trial(trial)

global exp;

%% Save trial variables to lists
exp.ARROWdata.stime(trial)            = exp.t;                              % Stimulus presentation time
exp.ARROWdata.catch_number            = exp.catch_numbers;

if ~isempty(exp.key)                                                        % If a keypress has been made
    exp.ARROWdata.key(trial)          = exp.key(1);                         % Which key has been pressed?
    exp.ARROWdata.keytime(trial)      = exp.keytime(1);                     % Keypress time
    exp.ARROWdata.RT(trial)           = exp.keytime(1) - exp.t;             % Time elapsed between cue onset (t) and time of key press (keytime)
    exp.ARROWdata.ACC(trial)          = exp.ACC;                            % 1 if correct; 0 if false; nan if no response
    exp.ARROWdata.mean_rotation(trial)= exp.mean_rotation(trial);           % Rotation of the normal arrows
    exp.ARROWdata.dev_rotation(trial) = exp.dev_rotation(trial);            % Deviance in rotation of the oddball arrow
else                                                                        % If no keypress, evything is nan
    exp.ARROWdata.mean_rotation(trial)= nan; 
    exp.ARROWdata.dev_rotation(trial) = nan;
    exp.ARROWdata.key(trial)          = nan;
    exp.ARROWdata.keytime(trial)      = nan;
    exp.ARROWdata.RT(trial)           = nan;
    exp.ARROWdata.ACC(trial)          = nan;  
end

if ~isempty(exp.catch_key)                                                  % If there was a catch trial and the subject gave a response
    exp.ARROWdata.catch_key(trial)    = exp.catch_key;
    exp.ARROWdata.catch_keytime(trial)= exp.catch_keytime;
    exp.ARROWdata.catch_RT(trial)     = exp.catch_keytime - exp.catch_t;
    exp.ARROWdata.catch_ACC(trial)    = exp.catch_ACC;
end

exp.key = [];
exp.catch_key = [];

%% Save results to file
result_file_name = [exp.results_filepath, sprintf('Results/ROT_%03s_%i_%s.mat', exp.subj, exp.session, exp.version)];
save(result_file_name, 'exp');
