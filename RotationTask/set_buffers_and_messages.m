function set_buffers_and_messages

global exp

%% Clear all buffers
for buffer = 1:9
    clearpict(buffer)
end

%% Name buffers
exp.buffer.G1                           = 1;
exp.buffer.G2                           = 1;
exp.buffer.fixation                     = 3;
exp.buffer.catch_trial                  = 4;
exp.buffer.correct_response             = 5;
exp.buffer.blank                        = 6;
exp.buffer.false_response               = 7;
exp.buffer.wrong_key                    = 8;
exp.buffer.no_response                  = 9;

%% Load feedback into feedback buffers (English and German)
if strcmp(exp.data.language, 'English')
    preparestring('good!', exp.buffer.correct_response);
    preparestring('false!', exp.buffer.false_response);
    preparestring('wrong key! (please use left and right arrow key)', exp.buffer.wrong_key);
    preparestring('no response!', exp.buffer.no_response);
else
    preparestring('gut!', exp.buffer.correct_response);
    preparestring('falsch!', exp.buffer.false_response);
    preparestring('Falsche Taste! (Benutze Links- und Rechtspfeil)', exp.buffer.wrong_key);
    preparestring('Keine Antwort!', exp.buffer.no_response);
end

%% Load fixation cross into buffer
preparestring('+', exp.buffer.fixation);

end